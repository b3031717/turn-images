package main

import (
	"fmt"
	"github.com/disintegration/imaging"
	"image/color"
	"io/ioutil"
	"os"
	"strings"
)
var (
	imageSourceDir = "source"
	imageOutPutDir = "output"
)

func main() {
	fmt.Println("turn image")
	// 建立來源資料夾
	if _, err := os.Stat(imageSourceDir); os.IsNotExist(err) {
		os.Mkdir(imageSourceDir, os.ModePerm)
	}
	// 建立輸出資料夾
	if _, err := os.Stat(imageOutPutDir); os.IsNotExist(err) {
		os.Mkdir(imageOutPutDir, os.ModePerm)
	}
	imageFiles , _ := ioutil.ReadDir("./" + imageSourceDir)

	for _, f := range imageFiles {
		makeImageTurn(f)
	}
}

func makeImageTurn(image os.FileInfo){
	var (
		savePath string // 儲存位置
	)
	path := fmt.Sprintf("%s/%s", "./"+imageSourceDir, image.Name())
	fileName := strings.Split(image.Name(), ".")

	// 建立單品資料夾
	if _, err := os.Stat(imageOutPutDir+"/"+fileName[0]); os.IsNotExist(err) {
		os.Mkdir(imageOutPutDir+"/"+fileName[0], os.ModePerm)
	}
	c := make(chan error)
	fmt.Println("start rotate ----> "+fileName[0])

	go func() {
		for start:=0.0 ; start < 360 ; start++ {
			savePath = fmt.Sprintf("%s/%s/%s_%d.%s", imageOutPutDir, fileName[0], fileName[0], int(start), fileName[1])
			fmt.Println(savePath)
			i, _ := imaging.Open(path, imaging.AutoOrientation(true))
			newImage := imaging.Rotate(i, start,  color.NRGBA{0, 0, 0, 0})
			c <- imaging.Save(newImage, savePath)
		}
		close(c)
	}()
	fmt.Printf("count channel error length ----> %d \n", len(c))
	for v := range c {
		if v != nil {
			fmt.Println(v)
		}
	}
}